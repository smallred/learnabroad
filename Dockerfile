FROM debian:stable
WORKDIR /data
COPY hugopub .
COPY app .
ENTRYPOINT [ "./main" ]

