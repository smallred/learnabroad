---
title: "Hello"
description: 
date: 2021-09-26T16:54:35+08:00
image: https://z3.ax1x.com/2021/09/26/465HRH.jpg
math: 
license: 
hidden: false
draft: false
toc: false
tags:
  - Start
---
# Hello！
## Welcome to my blog!
 Probably you have seen the description of the site. I am currently a Year 3 student in university. Next year I am gonna apply for the master program for further study. It is a long journey for preparing and apply for the master program. However, I believe the journey will be meaningful and memorable.
 This the first time to really record something. Initially, I will put everything, if it is worthy of recoding, on the blog. 

 I hope that someday in the future, when I come back to review all of these, I will say "Oh! Thankfully I wrote these down!".

**Let's go!**

[![To the Future](https://z3.ax1x.com/2021/09/26/4cXtIA.jpg)](https://imgtu.com/i/4cXtIA)