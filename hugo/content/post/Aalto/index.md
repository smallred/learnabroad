---
title: "Aalto University"
description: One of my favorite universities.
date: 2021-09-29T22:03:50+08:00
image: "1.jpeg"
math: 
license: 
hidden: false
draft: false
toc: true
categories:
  - Schools_Area
tags:
  - Aalto University
  - Finland
---
# Get to know: Aalto University
## Who is it?
>***Aalto University is where science and art meet technology and business. Our campus is located in Espoo, Greater Helsinki, Finland [1].***

For more information about Aalto University, please click [here](https://www.aalto.fi/en/aalto-university).

You can also have a virtual tour by clicking [here](https://virtualtour.aalto.fi/).

![Virtual Tour](2.png)

---

## Why I like it?
Before talking about the university, I wanna talk about the country, **Finland**. 

The first time I got to know Finland was reading a post on Zhihu. The post is comics named ***[Finnish Nightmares](https://book.douban.com/subject/26765619/)*** which shows how awkward finnishes will be in social occasions. 

![Finnish Nightmares](3.jpg)

To be honest, somehow I kind of like this atmosphere cuz I am also not so good at social contact. Sometimes I enjoy the loneness. The environment here fits me. I want to know more about the country, therefore, I begin to collect more information about the country.

I plan to go abroad to have my master program, so I also pay attention to the university in Finland. There are two top universities in Finland: **University of Helsinki** and **Aalto University**. I will introduce **University of Helsinki** in the later post. 

I mainly focus on the AI related majors and schools. I noticed two figures:

![THE World University Rankings 2020 by subject and NTU Rankings by subject 2021](4.png)

![NTU rankings by subject 2021](5.png)

Especially, I've heard that the Machine Learning courses are popular in the schools. 

Considering the overall environment and quality, it becomes one of my favorite universities. 



## Useful information about the application and majors

This section will be updated irregularly. 

[2021.10.13 Updated]

The registration Virtual Aalto Open Days now is open. Please visit [here](https://blog.mssz.xyz/p/aalto_virtual_open_2021/) to find more.
