---
title: "Get to know: IELTS"
description: This is a very detailed introduction of IELTS
date: 2021-10-01T22:52:32+08:00
image: "1.jpeg"
math: 
license: 
hidden: false
draft: false
toc: true
categories:
  - Language_Area
Tags:
  - IELTS
  - Introduction
---

# Introduction of IELTS

## What is it?

> The International English Language Testing System (IELTS) is designed to help you work, study or migrate to a country where English is the native language. This includes countries such as Australia, Canada, New Zealand, the UK and USA.
> 
> Your ability to listen, read, write and speak in English will be assessed during the test. IELTS is graded on a scale of 1-9. 
> 
> IELTS is jointly owned by the British Council, IDP: IELTS Australia and Cambridge Assessment English. [1]

## As a student, which IELTS type is right for me?

According to IELTS official website, there are two types of IELTS for studying: IELTS Academic and IELTS General Training [2]. The former one is for students who want to study in a university (higher education). Therefore, for most of students who want to study abroad, please choose IELTS Academic. For those who want to study below degree level or want to migrate to Australia, Canada, New Zealand and the UK, the latter one is right for them.

In this post we only focus on IELTS Academic.

## What is the test format?

There are four parts during the test: ***Listening, Reading, Writing and Speaking***.

In the following two sections, I will introduce the overall marking scheme and the specific information about these four parts.

## Overall marking scheme (The table is taken from the official website) [3]

| **Band score** | **Skill level**              | **Description**                                                                                                                                                                                                                                        |
| -------------- | ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **9**          | **Expert user**              | The test taker has fully operational command of the language. Their use of English is appropriate, accurate and fluent, and shows complete understanding.                                                                                              |
| **8**          | **Very good user**           | The test taker has fully operational command of the language with only occasional unsystematic inaccuracies and inappropriate usage. They may misunderstand some things in unfamiliar situations. They handle complex and detailed argumentation well. |
| **7**          | **Good user**                | The test taker has operational command of the language, though with occasional inaccuracies, inappropriate usage and misunderstandings in some situations. They generally handle complex language well and understand detailed reasoning.              |
| **6**          | **Competent user**           | The test taker has an effective command of the language despite some inaccuracies, inappropriate usage and misunderstandings. They can use and understand fairly complex language, particularly in familiar situations.                                |
| **5**          | **Modest user**              | The test taker has a partial command of the language and copes with overall meaning in most situations, although they are likely to make many mistakes. They should be able to handle basic communication in their own field.                          |
| **4**          | **Limited user**             | The test taker's basic competence is limited to familiar situations. They frequently show problems in understanding and expression. They are not able to use complex language.                                                                         |
| **3**          | **Extremely limited user**   | The test taker conveys and understands only general meaning in very familiar situations. There are frequent breakdowns in communication.                                                                                                               |
| **2**          | **Intermittent user**        | The test taker has great difficulty understanding spoken and written English.                                                                                                                                                                          |
| **1**          | **Non-user**                 | The test taker has no ability to use the language except a few isolated words.                                                                                                                                                                         |
| **0**          | **Did not attempt the test** | The test taker did not answer the questions.                                                                                                                                                                                                           |

For most of the universities, you have to achieve overall minimal **6.5** and satisfy the scores for Listening, Reading, Writing and Speaking. 

## Specific information about Listening, Reading, Writing and Speaking [4]

### Listening

**Time**: Approximately 30 minutes (+10min transfer time)

**Number of Sections**: 4

**Number of Questions**: 40

**Overall Scores**: 40

**Task Types**: multiple choice, matching, plan/map/diagram labelling, form/note/table/flow-chart/summary completion, sentence completion.

The first two parts deal with situations set in everyday social contexts. In Part 1, there is a conversation between two speakers (for example, a conversation about travel arrangements), and in Part 2, there is a monologue in (for example, a speech about local facilities). The final two parts deal with situations set in educational and training contexts. In Part 3, there is a conversation between two main speakers (for example, two university students in discussion, perhaps guided by a tutor), and in Part 4, there is a monologue on an academic subject.

**Test takers have 10 minutes to transfer their answers to the answer sheet at the end of the listening part.**

**For more information about the task type, please click [here](https://www.ielts.org/for-test-takers/test-format)**.

---

### Reading

**Time**: 60 minutes

**Number of Sections**: 3

**Number of Questions**: 40

**Overall Scores**: 40

**Task Types**: A variety of question types are used, chosen from the following; multiple choice, identifying information, identifying the writer’s views/claims, matching information, matching headings, matching features, matching sentence endings, sentence completion, summary completion, note completion, table completion, flow-chart completion, diagram label completion and short-answer questions.

**Test takers have to transfer their answers to the answer sheet during the time allowed for the reading part.**

**For more information about the task type, please click [here](https://www.ielts.org/for-test-takers/test-format)**.

---

### Writing

**Time**: 60 minutes

**Number of Sections**: 2

**Number of Questions**: 2

Task Types: In Task 1, test takers are asked to describe some visual information (graph/table/chart/diagram) in their own words. They need to write 150 words in about 20 minutes. In Task 2, they respond to a point of view or argument or problem. They need to write 250 words in about 40 minutes.

**Find more information about Writing, please click [here](https://www.ielts.org/for-test-takers/test-format).**

---

### Speaking

**Time**: 11-14 minutes

**Number of Sections**: 3

**Task Types**: There are three parts to the test and each part fulfils a specific function in terms of interaction pattern, task input and test takers output.

Find more about Speaking, Please click [here](https://www.ielts.org/for-test-takers/test-format).
