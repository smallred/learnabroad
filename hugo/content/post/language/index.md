---
title: "Language Area"
description: 
date: 2021-09-27T10:21:02+08:00
image: https://z3.ax1x.com/2021/09/27/4gBeit.jpg
math: 
license: 
hidden: false
draft: false
tags:
  - Language Test
  - Language Learning
  - Vocabulary
  - Writing
  - Listening
  - Speaking
Categories:
  - Language_Area
---
# Welcome to the Language Area!

This area is to write down some useful informain (e.g., vocabulary, test techniques, listening/writing materials, etc.).

You can click the above category and below tags to quickly find all the relative articles.